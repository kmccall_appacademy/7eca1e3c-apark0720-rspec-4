class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    [hours, minutes, m_seconds].map{ |int| add_zero(int) }.join(":")
  end

  def hours
    seconds / 3600
  end

  def minutes
    (seconds % 3600) / 60
  end

  def m_seconds
    (seconds % 60)
  end

  def add_zero(sec)
    sec.to_s.length == 1 ? "0#{sec}" : sec 
  end
end
