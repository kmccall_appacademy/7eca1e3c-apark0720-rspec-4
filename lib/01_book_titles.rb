class Book
  attr_reader :title

  def title=(title)
    output = []
    little_words = ["a", "an", "and", "the", "to", "of", "for",
    "by", "in", "out", "up", "down", "at", "with",
    "past", "over", "nor", "but", "or", "yet", "so"]

    title.split.each_with_index do |word, idx|
      output << word.capitalize if idx == 0
      next if idx == 0
      if little_words.include?(word)
        output << word
      else
        output << word.capitalize
      end
    end

    @title = output.join(" ")
  end

end
